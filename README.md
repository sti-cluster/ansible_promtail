# Ansible Role: promtail

## Description

Deploy [promtail](https://github.com/grafana/loki) using ansible. Supports amd64 and arm architectures.
